package com.boya.demo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.boya.demo.entity.Role;


/**
 * 用户DAO类
 */
@Repository
public interface RoleDao {

	 Role getById(Long id);
	
	 List<Role> getAll();
	
	 int insert(Role role);
	
	 int update(Role role);
	
	 void delete(Long id);
}
